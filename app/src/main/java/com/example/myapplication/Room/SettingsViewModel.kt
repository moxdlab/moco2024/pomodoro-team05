package com.example.myapplication.Room

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication.MyApplication
import com.example.myapplication.Room.DAO.SettingsDAO
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class SettingsViewModel() : ViewModel() {

    private val SettingsDAO: SettingsDAO = MyApplication.database?.settingsDAO() ?: throw Error("Error")
    var settingsDataList: LiveData<SettingsTimer> = getAllSettings()
    private val ioScope = CoroutineScope(Dispatchers.IO)

    fun getAllSettings(): LiveData<SettingsTimer> = SettingsDAO.getAllSettings()

    fun update(settingsTimer: SettingsTimer) {
        executeAsync {
            SettingsDAO.update(settingsTimer)
        }
    }

    suspend fun getSettingsIsEmpty(): Boolean {
        val count = SettingsDAO.getCount()
        return count == 0
    }

    fun updateSettingByEmpty() {
        executeAsync {
            if(getSettingsIsEmpty()) {
                SettingsDAO.insert(SettingsTimer())
            }
        }
    }

    private fun executeAsync(asyncCallBack: suspend () -> Unit) {
        ioScope.launch {
            try {
                asyncCallBack()
            } catch (error: Exception) {
                Log.e("Error-Setting", "$error")
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        ioScope.cancel()
    }
}