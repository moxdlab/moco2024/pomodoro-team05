package com.example.myapplication.Types

enum class NavigationType {
    Home,
    Settings,
    Timer
}