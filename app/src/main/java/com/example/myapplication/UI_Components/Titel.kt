package com.example.myapplication.UI_Components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.myapplication.UI_Components.Dialogs.TaskDialog
import com.example.myapplication.ui.theme.color_D9D9D9

@Composable
fun Title(titel: String, showButton: Boolean = false, onClickShowButton: () -> Unit = {}) {
    Column {
        Row (verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.SpaceBetween, modifier = Modifier.fillMaxWidth()) {
            Text("$titel:", fontSize = 20.sp)
            if (showButton) {
                Button(onClick = { onClickShowButton() }) {
                    Text(text = "New Task")
                }
            }
        }
        Spacer(
            Modifier
                .padding(0.dp, 5.dp)
                .fillMaxWidth()
                .height(3.dp)
                .background(color_D9D9D9))
    }
}