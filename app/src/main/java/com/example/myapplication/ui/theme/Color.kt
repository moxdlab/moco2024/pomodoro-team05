package com.example.myapplication.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)

//APP COLOR
val color_000000 = Color(0xFF000000)
val color_FFFFFF = Color(0xFFFFFFFF)
val color_F5F5F5 = Color(0xFFF5F5F5)
val color_E6E6FA = Color(0xFFE6E6FA)
val color_D9D9D9 = Color(0xFFD9D9D9)
val color_D9D9D9_30 = Color(0x4DD9D9D9) // 30% Deckkraft
val color_D9D9D9_80 = Color(0xCCD9D9D9) // 80% Deckkraft
val color_8A2BE2 = Color(0xFF8A2BE2)
val color_0066FF = Color(0xFF0066FF)
