import android.media.MediaPlayer
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.work.WorkInfo
import androidx.work.WorkManager
import com.example.myapplication.Helpers.ViewModelProvider
import com.example.myapplication.Room.SettingsTimer
import com.example.myapplication.Room.Task
import com.example.myapplication.UI_Components.Dialogs.MyAlertDialog
import com.example.myapplication.UI_Components.Title

@Composable
fun TimerScreen(todayTasks: List<Task>, settingsTimer: SettingsTimer, onClickEndSession: () -> Unit) {
    val appViewModel = ViewModelProvider.getAppViewModel()

    var showInfoDialog by remember { mutableStateOf(true) }
    val tasks by remember(todayTasks) { mutableStateOf(todayTasks) }
    val appSettings by remember(settingsTimer) { mutableStateOf(settingsTimer) }

    var isRunning by remember { mutableStateOf(false) }
    var isPause by remember { mutableStateOf(false) }
    var timeLeft by remember { mutableStateOf(appSettings.duration * 60) }
    var round by remember { mutableStateOf(1) }
    val context = LocalContext.current

    val mediaPlayer = remember { appViewModel.createMediaPlayer(appSettings.music, context) }

    val workManagerTimer by WorkManager.getInstance(context).getWorkInfosForUniqueWorkLiveData("pomodoroWork_ID").observeAsState(initial = null)

    workManagerTimer?.firstOrNull()?.let { workInfo ->
        if (workInfo.state == WorkInfo.State.RUNNING) {
            timeLeft = workInfo.progress.getInt("timeLeft", timeLeft)
            isPause = workInfo.progress.getBoolean("isPause", isPause)
            round = workInfo.progress.getInt("round", round)
        } else if (workInfo.state == WorkInfo.State.SUCCEEDED) {
            timeLeft = appSettings.duration * 60
            isPause = false
            isRunning = false
            round = 1
        }
    }

    if(tasks.isEmpty() && showInfoDialog) {
        MyAlertDialog(text = "Please make sure you have Task for Today!") {
            showInfoDialog = false
        }
    } else {
        showInfoDialog = false
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color(0xFFFAFAFA))
            .padding(16.dp)
    ) {
        // Timer-Anzeige
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 16.dp),
            contentAlignment = Alignment.Center
        ) {
            val text = if(isPause) "Pause" else "$round.Session"
            TimerDisplay(text ,timeLeft = timeLeft)
        }

        Spacer(modifier = Modifier.height(16.dp))

        Title(titel = "Today´s tasks:")

        TaskList(tasks = tasks)

        Spacer(modifier = Modifier.weight(1f))

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 16.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Button(
                onClick = {
                    if(isRunning) {
                        if (mediaPlayer?.isPlaying == true) {
                            mediaPlayer.apply {
                                pause()
                                isLooping = false
                            }
                        }
                        appViewModel.stopTimer(context)
                        isRunning = false

                    } else {
                        mediaPlayer?.apply{
                            start()
                            isLooping = true
                        }
                        appViewModel.runTimer(
                            context,
                            timeLeft,
                            appSettings.duration,
                            appSettings.shortBreak,
                            appSettings.longBreak,
                            round,
                            appSettings.longBreakAfterSessions
                        )
                        isRunning = true
                    }
                },
                modifier = Modifier.weight(1f),
                enabled = !isPause,
                colors = ButtonDefaults.buttonColors(containerColor = Color(0xFF7F56D9))
            ) {
                Icon(imageVector = Icons.Default.PlayArrow, contentDescription = null, tint = Color.White)
                Text(text = if(!isRunning) "Start" else "Pause", color = Color.White, modifier = Modifier.padding(start = 8.dp))
            }

            Spacer(modifier = Modifier.width(8.dp))

            Button(
                onClick = {
                    onClickEndSession()
                    appViewModel.destroyMediaPlayer()
                    appViewModel.stopTimer(context)
                },
                modifier = Modifier.weight(1f),
                colors = ButtonDefaults.buttonColors(containerColor = Color(0xFFFF5678))
            ) {
                Text(text = "End Session", color = Color.White)
            }
        }
    }
}

@Composable
fun TimerDisplay(text: String, timeLeft: Int) {
    val minutes = (timeLeft / 60).toString().padStart(2, '0')
    val seconds = (timeLeft % 60).toString().padStart(2, '0')

    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = "$minutes:$seconds",
            style = MaterialTheme.typography.displayLarge.copy(fontSize = 48.sp, fontWeight = FontWeight.Bold)
        )
        Text(
            text = text,
            style = MaterialTheme.typography.bodyMedium.copy(color = Color.Gray)
        )
    }
}

@Composable
fun TaskList(tasks: List<Task>) {
    val taskViewModel = ViewModelProvider.getTaskViewModel()
    var timerState by remember { mutableStateOf(tasks) }

    Column {
        timerState.forEachIndexed() { index, task ->
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 4.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = task.name,
                    style = MaterialTheme.typography.bodyLarge.copy(
                        textDecoration = if (task.isCompleted) TextDecoration.LineThrough else null,
                        color = if (task.isCompleted) Color.Gray else Color.Black
                    ),
                    modifier = Modifier.weight(1f)
                )
                Checkbox(
                    checked = task.isCompleted,
                    onCheckedChange = {
                        timerState = timerState.toMutableList().apply {
                            this[index] = this[index].copy(isCompleted = !this[index].isCompleted)
                        }

                        taskViewModel.updateTask(timerState[index])
                    }
                )
            }
        }
    }
}