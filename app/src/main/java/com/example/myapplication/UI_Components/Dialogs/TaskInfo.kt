package com.example.myapplication.UI_Components.Dialogs

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.myapplication.ui.theme.color_FFFFFF

@Composable
fun TaskInfoDialog(currentTaskDate: String, currentTaskName: String,showDialogDissmis: () -> Unit) {
    AlertDialog(onDismissRequest = { showDialogDissmis() }, containerColor = color_FFFFFF,
        title = { Text(text = "Task Info:") } ,
        text = {
               Column(modifier = Modifier.fillMaxWidth()) {
                   Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.SpaceBetween) {
                       Text(text = "Name:", fontSize = 15.sp, modifier = Modifier.width(50.dp), textAlign = TextAlign.Start)
                       Text(text = currentTaskName, fontSize = 15.sp, modifier = Modifier.width(180.dp), textAlign = TextAlign.Start)
                   }

                   Spacer(modifier = Modifier
                       .fillMaxWidth()
                       .height(10.dp))

                   Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.SpaceBetween) {
                       Text(text = "Date:", fontSize = 15.sp, modifier = Modifier.width(50.dp), textAlign = TextAlign.Start)
                       Text(text = currentTaskDate, fontSize = 15.sp,modifier = Modifier.width(180.dp), textAlign = TextAlign.Start)
                   }

                   Spacer(modifier = Modifier
                       .fillMaxWidth()
                       .height(10.dp))

                   Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.SpaceBetween) {
                       Text(text = "State:", fontSize = 15.sp, modifier = Modifier.width(50.dp), textAlign = TextAlign.Start)
                       Text(text = "finished", fontSize = 15.sp, modifier = Modifier.width(180.dp), textAlign = TextAlign.Start)
                   }
               }
        },
        confirmButton = {
        Button(modifier = Modifier
            .padding(0.dp, 0.dp, 10.dp, 0.dp)
            .width(90.dp), onClick = { showDialogDissmis() }) {
        Text(text = "Close")
    } })
}
