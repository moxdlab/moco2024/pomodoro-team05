package com.example.myapplication.Room

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication.Helpers.StringToDate
import com.example.myapplication.MyApplication
import com.example.myapplication.Room.DAO.TaskDAO
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class TaskViewModel(): ViewModel() {
    private val taskDAO: TaskDAO = MyApplication.database?.taskDAO() ?: throw Error("Error")
    var taskDataList: LiveData<List<Task>> = getAllSortedByDate()
    private val ioScope = CoroutineScope(Dispatchers.IO)

    fun getAllSortedByDate() = taskDAO.getAllSortedByDate()

    fun insert(taskName: String, taskDate:String) {
        executeAsync {
            val newTaskDate = StringToDate(taskDate)
            val newTask = Task(date = newTaskDate, name = taskName, isCompleted = false)
            taskDAO.insert(newTask)
        }
    }

    fun update(taskList: List<Task>) {
        executeAsync {
            taskList.forEach { taskDAO.update(it) }
        }
    }

    fun updateTaskById(taskId: Int, newTaskDate: String, newTaskName: String) {
        executeAsync {
            val task = taskDAO.getTaskByID(taskId)
            task.name = newTaskName
            task.date = StringToDate(newTaskDate)

            taskDAO.update(task)
        }
    }


    fun updateTask(task: Task) {
        executeAsync {
            taskDAO.update(task)
        }
    }

    fun delete(deletedTasks: List<Task>) {
        executeAsync {
            deletedTasks.forEach { taskDAO.delete(it) }
        }
    }

    private fun executeAsync(asyncCallBack: suspend () -> Unit) {
        ioScope.launch {
            try {
                asyncCallBack()
            } catch (error: Exception) {
                Log.e("Error-Tasks", "$error")
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        ioScope.cancel()
    }
}