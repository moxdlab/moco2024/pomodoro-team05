package com.example.myapplication.UI_Components.Settings

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.example.myapplication.Types.SettingsDropdownType
import com.example.myapplication.ui.theme.color_D9D9D9

@Composable
fun <T> SettingsDropdown(titel: String, content: T, listOfDropdownItem: List<T>, dropdownType: SettingsDropdownType = SettingsDropdownType.Minuten, onSelectedTimeChange: (time: T) -> Unit){
    var expanded by remember { mutableStateOf(false) }

    Row (
        Modifier
            .fillMaxWidth()
            .padding(0.dp, 5.dp), horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically){
        Text(text = titel)

        Box(){
            Row (modifier = Modifier
                .clickable { expanded = true }
                .border(BorderStroke(1.dp, color_D9D9D9)),horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically){
                Text(
                    text = contentName(content, dropdownType),
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .padding(5.dp)
                        .width(84.dp)
                )
                Icon(imageVector = Icons.Filled.ArrowDropDown, contentDescription = "ArrowDropDown")
            }
            DropdownMenu(
                expanded = expanded,
                onDismissRequest = { expanded = false },
                modifier = Modifier.width(117.dp)
            ) {
                listOfDropdownItem.forEach { time ->
                    DropdownMenuItem(text = { Text(text = contentName(time, dropdownType), modifier = Modifier.fillMaxWidth(), textAlign = TextAlign.Center) }, onClick = {
                        onSelectedTimeChange(time)
                        expanded = false
                    })
                }
            }
        }
    }

}

fun <T> contentName(content:T, dropdownType:SettingsDropdownType): String {
    return when(dropdownType) {
        SettingsDropdownType.Minuten -> "$content minutes"
        SettingsDropdownType.Einheiten -> "$content sessions"
        SettingsDropdownType.Musik -> "$content"
    }
}
