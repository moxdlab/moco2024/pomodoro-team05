package com.example.myapplication.UI_Components.Dialogs

import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Text
import androidx.compose.material3.Button
import androidx.compose.runtime.Composable
import com.example.myapplication.ui.theme.color_FFFFFF

@Composable
fun MyAlertDialog(
    text: String,
    onDismiss: () -> Unit
) {
    AlertDialog(
        onDismissRequest = { onDismiss() },
        containerColor = color_FFFFFF,
        text = { Text(text) },
        confirmButton = {
            Button(onClick = { onDismiss() }) {
                Text("close")
            }
        }
    )
}