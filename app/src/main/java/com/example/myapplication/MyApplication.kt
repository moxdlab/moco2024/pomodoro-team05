package com.example.myapplication

import android.app.Application
import com.example.myapplication.Helpers.createNotificationChannel
import com.example.myapplication.Room.MyDatabase

class MyApplication : Application() {
    companion object{
        var database: MyDatabase? = null
    }

    override fun onCreate() {
        super.onCreate()
        createNotificationChannel(this)
        database = MyDatabase.getDatabase(this)
    }

    override fun onTerminate() {
        super.onTerminate()
        database?.close()
    }
}