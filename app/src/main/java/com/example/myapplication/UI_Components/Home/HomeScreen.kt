package com.example.myapplication.UI_Components.Home

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.Button
import androidx.compose.material3.Checkbox
import androidx.compose.material3.CheckboxDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import com.example.myapplication.Helpers.ViewModelProvider
import com.example.myapplication.Room.Task
import com.example.myapplication.UI_Components.Dialogs.TaskDialog
import com.example.myapplication.UI_Components.Dialogs.TaskInfoDialog
import com.example.myapplication.UI_Components.Title
import java.util.Date
import com.example.myapplication.Helpers.dateToString
import com.example.myapplication.UI_Components.Dialogs.MyAlertDialog


// Home-Screen mit Listen für offene und erledigte Aufgaben
@Composable
fun HomeScreen(currentOpenTask: List<Task>, currentCompletedTask: List<Task>, onClickStartSession: () -> Unit) {
    val taskViewModel = ViewModelProvider.getTaskViewModel()

    var openTask by remember(currentOpenTask) { mutableStateOf(currentOpenTask.map { it.copy() }) }
    var completedTask by remember(currentCompletedTask) { mutableStateOf(currentCompletedTask.map { it.copy() }) }
    var changesEnabled by remember { mutableStateOf(false) }
    var showNewTaskDialog by remember { mutableStateOf(false) }
    var showInfoDialog by remember { mutableStateOf(false) }
    val currentTask by remember { mutableStateOf(Task()) }

    if(showNewTaskDialog && !changesEnabled) {
        TaskDialog(currentTask.id, dateToString(currentTask.date), currentTask.name) { showNewTaskDialog = false }
    }
    else if(showInfoDialog && !changesEnabled) {
        TaskInfoDialog(dateToString(currentTask.date), currentTask.name) { showInfoDialog = false }
    }
    else if ((showInfoDialog || showNewTaskDialog) && changesEnabled){
        MyAlertDialog("Please save or cancle your changes first!") { showNewTaskDialog = false; showInfoDialog = false }
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {
        Title(titel = "Open Tasks", showButton = true, onClickShowButton = {
            currentTask.id = 0
            currentTask.date = Date().time
            currentTask.name = ""
            showNewTaskDialog = true
        })
        TaskList(tasks = openTask,
            onTaskCompleted = {
                openTask = openTask.toMutableList().apply {
                    remove(it)
                }
                completedTask = completedTask.toMutableList().apply {
                    add(it.copy(isCompleted = true))
                }
                changesEnabled = true
            },
            onDeleteTask = {
                openTask -= it
                changesEnabled = true
            },
            onShowDialog = {
                currentTask.id = it.id
                currentTask.date = it.date
                currentTask.name = it.name
                showNewTaskDialog = true
            })

        Spacer(modifier = Modifier.height(16.dp))

        Title(titel = "Finished Tasks")
        TaskList(tasks = completedTask,
            onTaskCompleted = {
                completedTask = completedTask.toMutableList().apply {
                    remove(it)
                }
                openTask = openTask.toMutableList().apply {
                    add(it.copy(isCompleted = false))
                }
                changesEnabled = true
            },
            onDeleteTask = {
                completedTask -= it
                changesEnabled = true
            },
            onShowDialog = {
                currentTask.id = it.id
                currentTask.date = it.date
                currentTask.name = it.name
                showInfoDialog = true
            })

        Spacer(modifier = Modifier.height(16.dp))

        Row (verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.SpaceBetween, modifier = Modifier.fillMaxWidth()) {
            Button(onClick = {
                openTask = currentOpenTask.map { it.copy() }
                completedTask = currentCompletedTask.map { it.copy() }
                changesEnabled = false
                 }, enabled = changesEnabled){
                Text(text = "cancle")
            }
            Button(onClick = {
                val currentAllTask = currentOpenTask + currentCompletedTask
                val allList = openTask + completedTask

                if (currentAllTask.count() > allList.count()) {
                    val deletedTasks: List<Task> = currentAllTask.filter { !allList.contains(it) }
                    taskViewModel.delete(deletedTasks)
                }

                taskViewModel.update(allList)
                changesEnabled = false

            }, enabled = changesEnabled) {
                Text(text = "Save your changes")
            }
        }

        Spacer(modifier = Modifier.weight(1f))

        Button(onClick = { onClickStartSession() }, modifier = Modifier.fillMaxWidth()) {
            Text(text = "Start Session")
        }
    }

}

// Listendarstellung für Aufgaben
@Composable
fun TaskList(tasks: List<Task>, onTaskCompleted: (Task) -> Unit, onDeleteTask: (Task) -> Unit, onShowDialog: (Task) -> Unit) {
    LazyColumn {
        items(tasks) { task ->
            TaskItem(task = task, onTaskCompleted = onTaskCompleted, onDeleteTask= onDeleteTask, onShowDialog = onShowDialog)
        }
    }
}

// Ein einzelnes Element in der Liste der Aufgaben
@Composable
fun TaskItem(task: Task, onTaskCompleted: (Task) -> Unit, onDeleteTask: (Task) -> Unit, onShowDialog: (Task) -> Unit) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 8.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Column(modifier = Modifier
            .weight(1f)
            .clickable { onShowDialog(task) }) {
            Text(dateToString(task.date))
            Text(task.name)
        }
        IconButton(onClick = { onDeleteTask(task) }) {
            Icon(Icons.Default.Delete, contentDescription = null, tint = Color(0xFF7E57C2))
        }
        Checkbox(
            checked = task.isCompleted,
            onCheckedChange = {
                onTaskCompleted(task)
            },
            colors = CheckboxDefaults.colors(checkedColor = Color(0xFF7E57C2))
        )
    }
}
