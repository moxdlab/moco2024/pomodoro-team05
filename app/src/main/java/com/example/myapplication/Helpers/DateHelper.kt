package com.example.myapplication.Helpers

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

fun dateAreEqual(date1: Long,  date2: Long): Boolean = dateToString(date1) == dateToString(date2)

fun dateToString(times: Long):String {
    val date = Date(times)
    val dateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
    dateFormat.isLenient = false

    return dateFormat.format(date)
}

fun StringToDate(dateString: String):Long {
    val dateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
    dateFormat.isLenient = false
    val date = dateFormat.parse(dateString)
    return date.time
}
fun isValidDate(dateString: String): Boolean {
    try {
        val dateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
        dateFormat.isLenient = false
        dateFormat.parse(dateString)
    } catch (e: Exception) {
        return false
    }

    return true
}