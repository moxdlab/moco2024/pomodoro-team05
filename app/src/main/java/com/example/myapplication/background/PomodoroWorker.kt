package com.example.myapplication.background

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.example.myapplication.Helpers.hasNotificationPermission
import com.example.myapplication.Helpers.showNotification

class PomodoroWorker(context: Context, workerParams: WorkerParameters): Worker(context, workerParams) {

    override fun doWork(): Result {
        var remainingTime = inputData.getInt("remainingTime", 25)
        val appSettingsDuration = inputData.getInt("appSettingsDuration", 25) * 60
        val shortBreak =  inputData.getInt("shortBreak", 15) * 60
        val longBreak = inputData.getInt("longBreak", 25) * 60
        val currentRound = inputData.getInt("currentRound", 1)
        val sessionsUntilLongBreak = inputData.getInt("sessionsUntilLongBreak", 1)
        val isPause = inputData.getBoolean("isPause", false)

        var round = currentRound

        while (round <= sessionsUntilLongBreak) {
            showNotification(hasNotificationPermission(applicationContext), "$round.Session", "Pomodoro starts!. Let's GO!", applicationContext)
            runPomodoroPhase(remainingTime, appSettingsDuration, round,isPause)

            val breakDuration = if (currentRound == sessionsUntilLongBreak) longBreak else shortBreak
            showNotification(hasNotificationPermission(applicationContext), "Break", "Pomodoro End!. Time to take a break.", applicationContext)
            val resetTimer = runPomodoroPhase(breakDuration, appSettingsDuration, round, !isPause)

            if(resetTimer != null) remainingTime = resetTimer
            round += 1
        }

        return Result.success()
    }

    private fun runPomodoroPhase(duration: Int, appSettingsDuration:Int, round:Int, isPause: Boolean):Int? {
        var timeLeft = duration
        while (timeLeft >= 0) {
            if (!isStopped) {
                setProgressAsync(
                    workDataOf(
                        "round" to round,
                        "timeLeft" to timeLeft,
                        "isPause" to isPause
                    )
                )

                Thread.sleep(1000)
                timeLeft -= 1
            } else {
                return null
            }
        }

        return if(isPause) appSettingsDuration else null
    }
}