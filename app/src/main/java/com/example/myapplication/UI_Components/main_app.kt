package com.example.myapplication.UI_Components

import TimerScreen
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import com.example.myapplication.Helpers.ViewModelProvider
import com.example.myapplication.Helpers.dateAreEqual
import com.example.myapplication.Room.SettingsTimer
import com.example.myapplication.Room.Task
import com.example.myapplication.Types.NavigationType
import com.example.myapplication.UI_Components.Home.HomeScreen
import com.example.myapplication.UI_Components.Settings.Settings
import com.example.myapplication.ui.theme.color_F5F5F5
import java.util.Date

@Composable
fun main_app() {
    val appViewModel = ViewModelProvider.getAppViewModel()
    val settingsViewModel = ViewModelProvider.getSettingsViewModel()
    val taskViewModel = ViewModelProvider.getTaskViewModel()

    val selectedNavigation by appViewModel.selectedNavigation.observeAsState(initial = NavigationType.Home)
    val appSettings by settingsViewModel.settingsDataList.observeAsState(initial = SettingsTimer())
    val currentTasks by taskViewModel.taskDataList.observeAsState(initial = emptyList())
    val openTask: List<Task> = currentTasks.filter { !it.isCompleted }
    val completedTask: List<Task> = currentTasks.filter { it.isCompleted }
    val todaysTask:List<Task> = currentTasks.filter { dateAreEqual(it.date, Date().time) }

    Scaffold(
        bottomBar = {
            if (selectedNavigation != NavigationType.Timer) {
                BottomNavigationBar(selectedNavigation) { appViewModel.setSelectedNavigation(it) }
            }
        }
    ) { paddingValues ->
        Column (
            Modifier
                .fillMaxSize()
                .padding(paddingValues)
                .background(color = color_F5F5F5))
        {
            when(selectedNavigation) {
                NavigationType.Settings -> Settings(appSettings)
                NavigationType.Timer -> TimerScreen(todaysTask, appSettings) {
                    appViewModel.setSelectedNavigation(NavigationType.Home)
                }
                NavigationType.Home -> HomeScreen(openTask, completedTask) {
                    appViewModel.setSelectedNavigation(NavigationType.Timer)
                }
            }

        }

    }
}