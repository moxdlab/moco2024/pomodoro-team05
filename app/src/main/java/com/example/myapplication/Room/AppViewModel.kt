package com.example.myapplication.Room

import android.content.Context
import android.media.MediaPlayer
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.work.WorkManager
import com.example.myapplication.R
import com.example.myapplication.SaveDataClass
import com.example.myapplication.Types.NavigationType
import com.example.myapplication.background.WorkManagerPomodoro
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class AppViewModel: ViewModel() {
    var selectedNavigation = MutableLiveData(NavigationType.Home)
    var musicPlayer: MediaPlayer? = null
    private val ioScope = CoroutineScope(Dispatchers.IO)

    fun getSaveData(): SaveDataClass {
        return SaveDataClass(selectedNavigation.value!!)
    }

    fun runTimer(context: Context, workDuration: Int, appSettingsDuration:Int, shortBreak: Int, longBreak: Int, currentRound:Int, sessionsUntilLongBreak: Int) {
        executeAsync {
            WorkManagerPomodoro(
                context = context,
                remainingTime = workDuration,
                appSettingsDuration = appSettingsDuration,
                shortBreak = shortBreak,
                longBreak = longBreak,
                sessionsUntilLongBreak = sessionsUntilLongBreak,
                currentRound = currentRound,
            )
        }
    }

    fun stopTimer(context: Context) {
        executeAsync {
            WorkManager.getInstance(context).cancelAllWork()
        }
    }


    fun setSelectedNavigation(navigationType: NavigationType) {
        selectedNavigation.value = navigationType
    }

    fun destroyMediaPlayer() {
        musicPlayer?.apply {
            pause()
            isLooping = false
            stop()
            release()
        }
    }

    fun createMediaPlayer(music: String, context: Context): MediaPlayer? {
        if (music != "---") {
            musicPlayer = MediaPlayer.create(context, getMusic(music)).apply {
                setVolume(0.5f, 0.5f)
            }

            return musicPlayer
        }
            return null
    }

    fun getMusic(music:String):Int {
        return when(music) {
            "day" -> R.raw.day
            "farm" -> R.raw.farm
            "fire" -> R.raw.fire
            "night" -> R.raw.night
            "storm" -> R.raw.storm
            else -> throw Error("ERROR")
        }
    }

    private fun executeAsync(asyncCallBack: suspend () -> Unit) {
        ioScope.launch {
            try {
                asyncCallBack()
            } catch (error: Exception) {
                Log.e("Error-Setting", "$error")
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        ioScope.cancel()
        destroyMediaPlayer()
    }
}