package com.example.myapplication.Room.DAO

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.myapplication.Room.SettingsTimer
import com.example.myapplication.Room.Task

@Dao
interface SettingsDAO {
    @Insert
    suspend fun insert(settingsTimer: SettingsTimer)

    @Update
    suspend fun update(settingsTimer: SettingsTimer)

    @Query("SELECT * FROM TimerSetting LIMIT 1")
    fun getAllSettings(): LiveData<SettingsTimer>

    @Query("SELECT COUNT(*) FROM TimerSetting")
    suspend fun getCount(): Int
}