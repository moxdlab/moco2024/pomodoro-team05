package com.example.myapplication.Types

enum class SettingsDropdownType {
    Minuten,
    Einheiten,
    Musik
}