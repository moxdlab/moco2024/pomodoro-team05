package com.example.myapplication.Helpers

import androidx.compose.runtime.Composable
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.myapplication.Room.AppViewModel
import com.example.myapplication.Room.SettingsViewModel
import com.example.myapplication.Room.TaskViewModel

object ViewModelProvider {
    @Composable
    fun getSettingsViewModel() :SettingsViewModel = viewModel()

    @Composable
    fun getTaskViewModel() :TaskViewModel = viewModel()

    @Composable
    fun getAppViewModel() :AppViewModel = viewModel()
}