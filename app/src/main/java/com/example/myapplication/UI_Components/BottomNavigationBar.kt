package com.example.myapplication.UI_Components

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.Icon
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import com.example.myapplication.Types.NavigationType

@Composable
fun BottomNavigationBar(selectedNavigation: NavigationType, onClick: (selectedNavigation: NavigationType) -> Unit) {
    NavigationBar {
        NavigationBarItem(
            icon = { Icon(Icons.Filled.Home, contentDescription = "Home") },
            label = { Text("Home") },
            selected = selectedNavigation == NavigationType.Home,
            onClick = { onClick(NavigationType.Home) }
        )
        NavigationBarItem(
            icon = { Icon(Icons.Filled.Settings, contentDescription = "Settings") },
            label = { Text("Settings") },
            selected = selectedNavigation == NavigationType.Settings,
            onClick = { onClick(NavigationType.Settings)}
        )
    }
}