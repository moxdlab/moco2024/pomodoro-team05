package com.example.myapplication

import com.example.myapplication.Types.NavigationType

class SaveDataClass(nav: NavigationType = NavigationType.Home) {
    var selectedNavigation: NavigationType = nav
        set(value) { field = value }
}