package com.example.myapplication.UI_Components.Settings

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.myapplication.Helpers.ViewModelProvider
import com.example.myapplication.Room.SettingsTimer
import com.example.myapplication.Types.SettingsDropdownType
import com.example.myapplication.UI_Components.Title
import com.example.myapplication.ui.theme.color_0066FF

@Composable
fun Settings(appSettings: SettingsTimer) {
    val settingsViewModel = ViewModelProvider.getSettingsViewModel()
    val initialSettingsTimerState: SettingsTimer = appSettings
    var newSettingsTimerState by remember { mutableStateOf(initialSettingsTimerState) }
    var changesEnabled by remember { mutableStateOf(false) }

    Column(Modifier.padding(15.dp, 10.dp)) {
        Title(titel = "Promodoro-Timer")
        SettingsDropdown(
            titel = "Duration",
            content = newSettingsTimerState.duration,
            listOfDropdownItem = listOf(25, 15, 10, 5, 1)
        ) {
            newSettingsTimerState = newSettingsTimerState.copy(duration = it)
            changesEnabled = true
        }

        SettingsDropdown(
            titel = "short break",
            content = newSettingsTimerState.shortBreak,
            listOfDropdownItem = listOf(15, 10, 5, 1)
        ) {
            newSettingsTimerState = newSettingsTimerState.copy(shortBreak = it)
            changesEnabled = true
        }

        SettingsDropdown(
            titel = "Long break",
            content = newSettingsTimerState.longBreak,
            listOfDropdownItem = listOf(25, 20, 15, 10, 1)
        ) {
            newSettingsTimerState = newSettingsTimerState.copy(longBreak = it)
            changesEnabled = true
        }

        SettingsDropdown(
            titel = "Long break after",
            content = newSettingsTimerState.longBreakAfterSessions,
            listOfDropdownItem = listOf(2, 4, 6, 8, 1),
            SettingsDropdownType.Einheiten
        ) {
            newSettingsTimerState = newSettingsTimerState.copy(longBreakAfterSessions = it)
            changesEnabled = true
        }

        Spacer(modifier = Modifier
            .fillMaxWidth()
            .height(50.dp))

        Title(titel = "Ringtone")
        SettingsDropdown(
            titel = "Music",
            content = newSettingsTimerState.music,
            listOfDropdownItem = listOf("---", "day", "farm", "fire", "night", "storm"),
            SettingsDropdownType.Musik
        ) {
            newSettingsTimerState = newSettingsTimerState.copy(music = it)
            changesEnabled = true
        }

        Spacer(modifier = Modifier
            .fillMaxWidth()
            .height(50.dp))

        Title(titel = "About")
        SettingsInfo(titel = "Developed by", content = "Emirhan, Kaan, Zara")
        SettingsInfo(titel = "Download in", content = "Android")
        SettingsInfo(titel = "Support", content = "email@support-timer.com", color = color_0066FF)

        Spacer(modifier = Modifier.height(20.dp))

        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
        ) {
            Button(
                onClick = {
                    newSettingsTimerState = initialSettingsTimerState
                    changesEnabled = false
                },
                enabled = changesEnabled
            ) {
                Text(text = "cancel")
            }
            Button(
                onClick = {
                    settingsViewModel.update(newSettingsTimerState)
                    changesEnabled = false
                },
                enabled = changesEnabled)
            {
                Text(text = "Save your changes")
            }
        }
    }
}
