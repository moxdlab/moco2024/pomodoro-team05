package com.example.myapplication.Room.DAO

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.myapplication.Room.Task

@Dao
interface TaskDAO {
    @Insert
    suspend fun insert(task: Task)

    @Update
    suspend fun update(task: Task)

    @Delete
    suspend fun delete(task: Task)

    @Query("SELECT * FROM Task ORDER BY date ASC")
    fun getAllSortedByDate(): LiveData<List<Task>>

    @Query("SELECT * FROM Task WHERE id = (:taskId)")
    suspend fun getTaskByID(taskId: Int): Task
}