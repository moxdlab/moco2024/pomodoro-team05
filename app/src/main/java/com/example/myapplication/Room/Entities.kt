package com.example.myapplication.Room

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Date

@Entity(tableName = "Task")
data class Task(
    @PrimaryKey(autoGenerate = true) var id:Int = 0,
    var date: Long = Date().time,
    var name: String = "",
    var isCompleted: Boolean = false
)
@Entity(tableName = "TimerSetting")
data class SettingsTimer(
    @PrimaryKey val id: Int = 1,
    var duration: Int = 25,
    var shortBreak: Int = 15,
    var longBreak: Int = 25,
    var longBreakAfterSessions: Int = 4,
    var music: String = "day"
)