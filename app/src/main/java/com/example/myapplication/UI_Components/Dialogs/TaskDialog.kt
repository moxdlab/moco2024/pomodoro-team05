package com.example.myapplication.UI_Components.Dialogs

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.myapplication.Helpers.ViewModelProvider
import com.example.myapplication.Helpers.isValidDate
import com.example.myapplication.ui.theme.color_FFFFFF

@Composable
fun TaskDialog(currentTaskId: Int, currentTaskDate: String, currentTaskName: String, showDialogDissmis: () -> Unit) {
    val taskViewModel = ViewModelProvider.getTaskViewModel()
    var taskDate by remember { mutableStateOf(currentTaskDate) }
    var taskName by remember { mutableStateOf(currentTaskName) }
    var taskFormularError by remember { mutableStateOf(false) }

    AlertDialog(
        onDismissRequest = {
            taskDate = ""
            taskName = ""
            showDialogDissmis()
        },
        containerColor = color_FFFFFF,
        title = { Text(text = "Task Formluar:") },
        text = {
            Column(modifier = Modifier.fillMaxWidth()) {
                Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.SpaceBetween) {
                    Text(text = "Name:", fontSize = 15.sp, modifier = Modifier.width(65.dp), textAlign = TextAlign.Start)
                    TextField(value = taskName, onValueChange = {taskName = it}, isError = taskFormularError, placeholder = { Text(
                        text = "Taskname ..."
                    )} )
                }

                Spacer(modifier = Modifier
                    .fillMaxWidth()
                    .height(10.dp))

                Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.SpaceAround) {
                    Text(text = "Date:", fontSize = 15.sp, modifier = Modifier.width(65.dp), textAlign = TextAlign.Start)
                    TextField(value = taskDate, onValueChange = {taskDate = it}, isError = taskFormularError, placeholder = { Text(
                        text = "DD.MM.YYYY"
                    )})
                }

                Spacer(modifier = Modifier
                    .fillMaxWidth()
                    .height(10.dp))

                Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.SpaceBetween) {
                    Text(text = "State:", fontSize = 15.sp, modifier = Modifier.width(50.dp), textAlign = TextAlign.Start)
                    Text(text = "not finished yet", fontSize = 15.sp, modifier = Modifier.width(200.dp), textAlign = TextAlign.Start)
                }
            }
        },
        dismissButton = { Button(modifier = Modifier
            .padding(0.dp, 0.dp, 40.dp, 0.dp)
            .width(90.dp),onClick = {
            showDialogDissmis()
        }) {
            Text(text = "cancle")
        } },

        confirmButton = { Button(modifier = Modifier
            .padding(0.dp, 0.dp, 20.dp, 0.dp)
            .width(90.dp),onClick = {
            if (isValidDate(taskDate) && taskName.isNotEmpty() && currentTaskName.isNotEmpty()) {
                taskViewModel.updateTaskById(currentTaskId, taskDate, taskName)
                showDialogDissmis()

            } else if(isValidDate(taskDate) && taskName.isNotEmpty() && currentTaskName.isEmpty()) {
                taskViewModel.insert(taskName, taskDate)
                showDialogDissmis()

            } else {
                taskFormularError = true
            }
        }) {
                Text(text = "save") }
        },
    )
}