package com.example.myapplication.Room

import android.content.Context
import androidx.room.Room
import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.myapplication.Room.DAO.SettingsDAO
import com.example.myapplication.Room.DAO.TaskDAO

@Database(entities = [Task::class, SettingsTimer::class], version = 1)
abstract class MyDatabase: RoomDatabase() {

    abstract fun taskDAO() : TaskDAO
    abstract fun settingsDAO() :SettingsDAO

    companion object {
        @Volatile
        private var Instance: MyDatabase? = null

        fun getDatabase(context: Context): MyDatabase {
            return Instance ?: synchronized(this) {
                Room.databaseBuilder(context, MyDatabase::class.java, "My_DataBase")
                    .build()
                    .also { Instance = it }
            }
        }
    }

}