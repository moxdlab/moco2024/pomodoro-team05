package com.example.myapplication.background

import android.content.Context
import androidx.work.Data
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager

fun WorkManagerPomodoro(context: Context, remainingTime: Int, appSettingsDuration: Int, shortBreak: Int, longBreak: Int, sessionsUntilLongBreak: Int, currentRound:Int) {
    val workRequest = OneTimeWorkRequest.Builder(PomodoroWorker::class.java)
        .setInputData(
            Data.Builder()
                .putInt("remainingTime", remainingTime)
                .putInt("appSettingsDuration", appSettingsDuration)
                .putInt("shortBreak", shortBreak)
                .putInt("longBreak", longBreak)
                .putInt("currentRound", currentRound)
                .putInt("sessionsUntilLongBreak", sessionsUntilLongBreak)
                .build()
        )
        .build()

    WorkManager.getInstance(context).enqueueUniqueWork(
        "pomodoroWork_ID",
        ExistingWorkPolicy.REPLACE,
        workRequest
    )
}
