package com.example.myapplication

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.myapplication.UI_Components.main_app
import com.example.myapplication.ui.theme.MyApplicationTheme
import android.Manifest
import android.os.Build
import android.util.Log
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.lifecycleScope
import com.example.myapplication.Helpers.hasNotificationPermission
import com.example.myapplication.Helpers.showNotification
import com.example.myapplication.Room.AppViewModel
import com.example.myapplication.Room.SettingsViewModel
import com.example.myapplication.Room.TaskViewModel
import com.example.myapplication.Types.NavigationType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {
    private val settingsViewModel :SettingsViewModel by viewModels()
    private val taskViewModel :TaskViewModel by viewModels()
    private val appViewModel :AppViewModel by viewModels()
    private var savedDataClass = SaveDataClass()
    private val mainScope = CoroutineScope(Dispatchers.Main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            settingsViewModel.updateSettingByEmpty()

            val context = LocalContext.current
            var hasPermission by remember { mutableStateOf(hasNotificationPermission(context)) }
            val permissionLauncher = rememberLauncherForActivityResult(
                contract = ActivityResultContracts.RequestPermission(),
                onResult = { isGranted ->
                    hasPermission = isGranted
                    showNotification(hasPermission,"Success Notification", "You have successfully enabled notifications. Stay tuned for updates and alerts!", context)
                }
            )

            executeAsync {
                if (!hasPermission && Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                    permissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
                }
            }


            MyApplicationTheme {
                Surface(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(8.dp),
                    color = MaterialTheme.colorScheme.background
                ) {
                    main_app()
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        executeAsync {
            taskViewModel.getAllSortedByDate()
            settingsViewModel.getAllSettings()
        }
    }

    override fun onRestart() {
        super.onRestart()
        executeAsync {
            appViewModel.setSelectedNavigation(savedDataClass.selectedNavigation)
        }
    }

    override fun onStop() {
        super.onStop()
        executeAsync {
            val data = appViewModel.getSaveData()
            savedDataClass.selectedNavigation = data.selectedNavigation
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        executeAsync {
            appViewModel.setSelectedNavigation(NavigationType.Home)
            appViewModel.stopTimer(applicationContext)
        }

        mainScope.cancel()
    }

    private fun executeAsync(asyncCallBack: () -> Unit) {
        mainScope.launch {
            try {
                asyncCallBack()
            } catch (error: Exception) {
                Log.e("Error-Setting", "$error")
            }
        }
    }
}
